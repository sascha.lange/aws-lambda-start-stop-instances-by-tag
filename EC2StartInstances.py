# Start all instances in all regions with a specific tag
import boto3


def lambda_handler(event, context):
    ec2 = boto3.client('ec2')

    # Get list of regions
    region_list = ec2.describe_regions().get('Regions',[] )

    # Filter instances by this tag
    custom_filter = [{
        'Name':'tag:Shutdown_at_night',
        'Values': ['yes']}]

    for region in region_list:
        print "Checking region %s " % region['RegionName']
        reg=region['RegionName']

        # Connect to region
        ec2 = boto3.client('ec2', region_name=reg)

        response = ec2.describe_instances(Filters=custom_filter)

        instance_list = []

        for i in response['Reservations']:
            for x in i['Instances']:
                instance_list.append(x['InstanceId'])

        ec2.start_instances(InstanceIds=instance_list)
        print 'started your instances: ' + str(instance_list)
